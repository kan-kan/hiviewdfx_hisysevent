# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/test.gni")

module_output_path = "hisysevent_native/hisysevent_native"

config("hisysevent_native_test_config") {
  visibility = [ ":*" ]

  include_dirs = [ "include" ]
}

ohos_moduletest("HiSysEventAdapterNativeTest") {
  module_out_path = module_output_path

  sources = [ "hisysevent_adapter_native_test.cpp" ]

  configs = [ ":hisysevent_native_test_config" ]

  deps = [
    "../../../adapter/native/idl:sys_event_impl_client",
    "../../../adapter/native/idl:sys_event_service_gen_src_client",
    "../../../frameworks/native/util:hisysevent_util",
    "../../../interfaces/native/innerkits/hisysevent:hisysevent_static_lib_for_tdd",
    "../../../interfaces/native/innerkits/hisysevent_manager:hisyseventmanager_static_lib_for_tdd",
    "//third_party/googletest:gtest_main",
  ]

  if (is_standard_system) {
    external_deps = [
      "c_utils:utils",
      "hilog:libhilog",
      "ipc:ipc_single",
    ]
  } else {
    external_deps = [
      "c_utils:utils",
      "hilog:libhilog",
      "ipc:ipc_single",
    ]
    cflags_cc = [ "-D__HIVIEW_HAVE_HITRACE__" ]
  }
}

ohos_moduletest("HiSysEventNativeTest") {
  module_out_path = module_output_path

  sources = [ "hisysevent_native_test.cpp" ]

  configs = [ ":hisysevent_native_test_config" ]

  deps = [
    "../../../adapter/native/idl:sys_event_impl_client",
    "../../../adapter/native/idl:sys_event_service_gen_src_client",
    "../../../interfaces/native/innerkits/hisysevent:hisysevent_static_lib_for_tdd",
    "../../../interfaces/native/innerkits/hisysevent/encode:hisysevent_encoded",
    "../../../interfaces/native/innerkits/hisysevent_manager:hisyseventmanager_static_lib_for_tdd",
    "//third_party/googletest:gtest_main",
  ]

  if (is_standard_system) {
    external_deps = [
      "c_utils:utils",
      "hilog:libhilog",
    ]
  } else {
    external_deps = [
      "c_utils:utils",
      "hilog:libhilog",
    ]
    cflags_cc = [ "-D__HIVIEW_HAVE_HITRACE__" ]
  }
}

ohos_moduletest("HiSysEventCTest") {
  module_out_path = module_output_path

  sources = [ "hisysevent_c_test.cpp" ]

  configs = [ ":hisysevent_native_test_config" ]

  if (build_public_version) {
    deps = [ "//third_party/bounds_checking_function:libsec_shared" ]
  } else {
    deps = [ "//third_party/bounds_checking_function:libsec_static" ]
  }

  external_deps = [
    "hilog:libhilog",
    "hisysevent:libhisysevent",
  ]
}

ohos_moduletest("HiSysEventManagerCTest") {
  module_out_path = module_output_path

  include_dirs =
      [ "//base/hiviewdfx/hisysevent/adapter/native/idl/include/ret_code.h" ]

  sources = [ "hisysevent_manager_c_test.cpp" ]

  configs = [ ":hisysevent_native_test_config" ]

  deps = [
    "../../../frameworks/native/util:hisysevent_util",
    "../../../interfaces/native/innerkits/hisysevent_manager:hisyseventmanager_static_lib_for_tdd",
    "//third_party/googletest:gtest_main",
  ]

  if (build_public_version) {
    deps += [ "//third_party/bounds_checking_function:libsec_shared" ]
  } else {
    deps += [ "//third_party/bounds_checking_function:libsec_static" ]
  }

  external_deps = [ "hilog:libhilog" ]
}

ohos_moduletest("HiSysEventDelayTest") {
  module_out_path = module_output_path

  include_dirs =
      [ "//base/hiviewdfx/hisysevent/adapter/native/idl/include/ret_code.h" ]

  sources = [ "hisysevent_delay_test.cpp" ]

  configs = [ ":hisysevent_native_test_config" ]

  deps = [
    "../../../frameworks/native/util:hisysevent_util",
    "../../../interfaces/native/innerkits/hisysevent:hisysevent_static_lib_for_tdd",
    "//third_party/googletest:gtest_main",
  ]

  if (build_public_version) {
    deps += [ "//third_party/bounds_checking_function:libsec_shared" ]
  } else {
    deps += [ "//third_party/bounds_checking_function:libsec_static" ]
  }

  external_deps = [ "hilog:libhilog" ]
}

ohos_moduletest("HiSysEventWroteResultCheckTest") {
  module_out_path = module_output_path

  include_dirs =
      [ "//base/hiviewdfx/hisysevent/adapter/native/idl/include/ret_code.h" ]

  sources = [ "hisysevent_wrote_result_check_test.cpp" ]

  configs = [ ":hisysevent_native_test_config" ]

  deps = [
    "../../../frameworks/native/util:hisysevent_util",
    "../../../interfaces/native/innerkits/hisysevent:hisysevent_static_lib_for_tdd",
    "../../../interfaces/native/innerkits/hisysevent_manager:hisyseventmanager_static_lib_for_tdd",
    "//third_party/googletest:gtest_main",
  ]

  if (build_public_version) {
    deps += [ "//third_party/bounds_checking_function:libsec_shared" ]
  } else {
    deps += [ "//third_party/bounds_checking_function:libsec_static" ]
  }

  external_deps = [
    "hilog:libhilog",
    "hitrace:libhitracechain",
  ]
}

ohos_moduletest("HiSysEventEncodedTest") {
  module_out_path = module_output_path

  include_dirs =
      [ "//base/hiviewdfx/hisysevent/adapter/native/idl/include/ret_code.h" ]

  sources = [ "hisysevent_encoded_test.cpp" ]

  configs = [ ":hisysevent_native_test_config" ]

  deps = [
    "../../../frameworks/native/util:hisysevent_util",
    "../../../interfaces/native/innerkits/hisysevent:hisysevent_static_lib_for_tdd",
    "//third_party/googletest:gtest_main",
  ]

  if (build_public_version) {
    deps += [ "//third_party/bounds_checking_function:libsec_shared" ]
  } else {
    deps += [ "//third_party/bounds_checking_function:libsec_static" ]
  }

  external_deps = [
    "hilog:libhilog",
    "hitrace:libhitracechain",
  ]
}

group("moduletest") {
  testonly = true
  deps = []

  deps += [
    ":HiSysEventAdapterNativeTest",
    ":HiSysEventCTest",
    ":HiSysEventDelayTest",
    ":HiSysEventEncodedTest",
    ":HiSysEventManagerCTest",
    ":HiSysEventNativeTest",
    ":HiSysEventWroteResultCheckTest",
  ]
}
